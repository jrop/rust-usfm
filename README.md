# Rust USFM Tools

* Lexer
* Parser
* Reader? (TODO)
* WASM? (TODO)
* JS-bindings? (TODO)

# TODO

The way paragraphs are parsed is not satisfactory: `\p` is parsed as a
standalone tag, and what we likely want to do is parse until the next `\p` (or
lower precedence).  We can steal the precedence portion of Pratt parsing for
this (but we likely don't need the LED/NUD concept).

## Terminal Renderer

Render Chapter numbers with ASCII Fonts.  These fonts work well because
they are not excessively large (http://www.patorjk.com/software/taag):

* AMC 3 Line
* Cygnet
* Digital

ASCII fonts: FIGlet/AOL Macro Fonts.
