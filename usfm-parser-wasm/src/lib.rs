use usfm_parser::{lexer, parser};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn parse(s: &str) -> JsValue {
    let mut lex = lexer::TokenIter::from_str(s);
    let ast = parser::parse(&mut lex);
    match JsValue::from_serde(&ast) {
        Ok(v) => v,
        Err(e) => wasm_bindgen::throw_str(&e.to_string()),
    }
}
