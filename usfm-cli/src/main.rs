use clap::Clap;
use std::collections::HashSet;
use usfm_parser::{lexer, parser};

#[derive(Clap)]
struct CliOpt {
    #[clap(subcommand)]
    subcommand: CliSubCommand,
}

#[derive(Clap)]
enum CliSubCommand {
    ParseAll,
    Parse(CliSubcommandParse),
}

#[derive(Clap)]
struct CliSubcommandParse {
    file: String,
}

#[allow(unused)]
fn visit(node: &parser::Node, attributes: &mut HashSet<i16>) {
    //{{{
    match node {
        parser::Node::TextAndAttributes {
            text: _,
            attributes: attrs,
        } => {
            for (k, v) in attrs {
                if *k != "strong" {
                    continue;
                }
                let v = v.trim_matches('"');
                let multiplier = match v.chars().next() {
                    Some('H') => -1_i16,
                    Some('G') => 1_i16,
                    c => panic!("unsupported attribute prefix: {:?} in {:?}", c, v),
                };
                let n: i16 = v
                    .chars()
                    .skip(1)
                    .collect::<String>()
                    .parse()
                    .expect("could not parse");
                let n = n * multiplier;
                attributes.insert(n);
            }
        }
        parser::Node::Group { parent, children } => {
            visit(parent, attributes);
            for child in children {
                visit(child, attributes)
            }
        }
        parser::Node::List { children } => {
            for child in children {
                visit(child, attributes)
            }
        }
        parser::Node::Pair { node1, node2 } => {
            visit(node1, attributes);
            visit(node2, attributes);
        }
        _ => {}
    }
} //}}}

fn parse_all() {
    //{{{
    use std::io::Write;
    use std::convert::TryFrom;

    // let mut attributes = HashSet::new();
    for file in std::fs::read_dir("./web/").unwrap() {
        let file = format!("./web/{}", file.unwrap().file_name().to_str().unwrap());
        if !file.ends_with(".usfm") {
            continue;
        }
        print!("parsing {}...", file);
        std::io::stdout().flush().unwrap();
        let usfm_contents = std::fs::read_to_string(file).unwrap();
        let mut lex = lexer::TokenIter::from_str(&usfm_contents);
        let ast = parser::parse(&mut lex);
        let _ast = usfm_parser::ast_compact::Node::try_from(&ast).expect("could not simplify AST");
        // visit(&ast, &mut attributes);

        println!("done!");
    }
    println!("Done!");
    // println!("{:?}", attributes);
    // println!("{}", attributes.len());
} //}}}

fn main() {
    let cli_opts: CliOpt = CliOpt::parse();
    match cli_opts.subcommand {
        CliSubCommand::ParseAll => parse_all(),
        CliSubCommand::Parse(cmd) => {
            let source = std::fs::read_to_string(&cmd.file)
                .unwrap_or_else(|e| panic!("could not read {}: {}", &cmd.file, e.to_string()));
            let mut lex = lexer::TokenIter::from_str(&source);
            let ast = parser::parse(&mut lex);
            let ast = serde_json::to_string_pretty(&ast).expect("could not convert AST to JSON");
            println!("{}", ast);
        }
    }
}
