use crate::unicode::UnicodeString;
use std::iter::Iterator;

#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind {
    EOF,
    Tag,
    Text,
}
#[derive(Clone, Debug, PartialEq)]
pub struct Token<'a> {
    pub kind: TokenKind,
    pub text: &'a str,
}
impl<'a> Token<'a> {
    pub fn new(kind: TokenKind, text: &'a str) -> Self {
        Token { kind, text }
    }
}

pub struct TokenIter<'a> {
    ustr: UnicodeString<'a>,
}
impl<'a> TokenIter<'a> {
    pub fn from_str(source: &'a str) -> Self {
        let ustr = UnicodeString::new(source);
        TokenIter { ustr }
    }
    pub fn ustr(&mut self) -> &mut UnicodeString<'a> {
        &mut self.ustr
    }
}
impl<'a> Iterator for TokenIter<'a> {
    type Item = Token<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let tkn = read_at(&mut self.ustr);
        match tkn.kind {
            TokenKind::EOF => None,
            _ => Some(tkn),
        }
    }
}

impl<'a> TokenIter<'a> {
    #[allow(dead_code)]
    pub fn expect_any<T: AsRef<[TokenKind]>>(&mut self, kinds: T) -> Token<'a> {
        let kinds = kinds.as_ref();
        let tkn = self.next();
        if !kinds.iter().any(|kind| &tkn.kind == kind) {
            panic!("Expected {:?}, but got {:?}", kinds, tkn.kind);
        }
        tkn
    }

    pub fn expect(&mut self, kind: TokenKind) -> Token<'a> {
        let tkn = self.next();
        if tkn.kind != kind {
            panic!("Expected {:?}, but got {:?}", kind, tkn);
        }
        tkn
    }

    #[allow(dead_code)]
    pub fn expect_f<F: Fn(&Token<'a>) -> bool>(&mut self, f: F) -> Token<'a> {
        let tkn = self.next();
        if !f(&tkn) {
            panic!("Unexpected token: {:?}", tkn);
        }
        tkn
    }

    pub fn next(&mut self) -> Token<'a> {
        read_at(&mut self.ustr)
    }

    pub fn peek(&self) -> Token<'a> {
        read_at(&mut self.ustr.clone())
    }
}

pub fn read_at<'a>(ustr: &mut UnicodeString<'a>) -> Token<'a> {
    match ustr.peek() {
        Some((_, "\\")) => {
            let mut first_call = true;
            let mut hit_star = false;
            let text = &ustr.peeking_take_while(|(_, s)| {
                if hit_star {
                    return false;
                }
                if first_call {
                    first_call = false;
                    return true;
                }
                if s.chars().all(|c| c.is_alphanumeric() || *s == "+") {
                    return true;
                }
                if s == &"*" {
                    hit_star = true;
                    return true;
                }
                false
            });
            Token::new(TokenKind::Tag, text)
        }
        Some(_) => {
            let text = &ustr.peeking_take_while(|(_, s)| !s.starts_with('\\'));
            Token::new(TokenKind::Text, text)
        }
        None => Token::new(TokenKind::EOF, ""),
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_lexer() {
        let source = "\\c 1 \\w This \\w* would be chapter 1 \\c 2";
        let tokens: Vec<Token> = TokenIter::from_str(source).collect();
        assert_eq!(
            tokens,
            [
                Token {
                    kind: TokenKind::Tag,
                    text: "\\c".into(),
                },
                Token {
                    kind: TokenKind::Text,
                    text: " 1 ".into(),
                },
                Token {
                    kind: TokenKind::Tag,
                    text: "\\w".into(),
                },
                Token {
                    kind: TokenKind::Text,
                    text: " This ".into(),
                },
                Token {
                    kind: TokenKind::Tag,
                    text: "\\w*".into(),
                },
                Token {
                    kind: TokenKind::Text,
                    text: " would be chapter 1 ".into(),
                },
                Token {
                    kind: TokenKind::Tag,
                    text: "\\c".into(),
                },
                Token {
                    kind: TokenKind::Text,
                    text: " 2".into(),
                }
            ]
        )
    }

    #[test]
    fn test_unicode() {
        let unicode_string =
            r"\f + \fr 1:1  \ft The Hebrew word rendered “God” is “אֱלֹהִ֑ים” (Elohim).\f* \p";
        let _tokens: Vec<Token> = TokenIter::from_str(unicode_string).collect();
    }
}
