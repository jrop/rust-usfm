use itertools::PeekingNext;
use std::iter::Iterator;
use unicode_segmentation::GraphemeIndices;

#[derive(Clone)]
pub struct UnicodeString<'a> {
    pub source: &'a str,
    position: usize,
    graphemes: GraphemeIndices<'a>,
    peeked: Option<Option<(usize, &'a str)>>,
}
impl<'a> UnicodeString<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source,
            position: 0,
            graphemes: unicode_segmentation::UnicodeSegmentation::grapheme_indices(source, false),
            peeked: None,
        }
    }

    #[allow(dead_code)]
    pub fn tellp(&self) -> usize {
        self.position
    }

    pub fn peek(&mut self) -> Option<&(usize, &'a str)> {
        match self.peeked {
            Some(ref p) => p.as_ref(),
            None => {
                let pos = self.position;
                let next = self.next();
                self.position = pos;
                self.peeked.replace(next);
                self.peek()
            }
        }
    }

    pub fn peeking_take_while<F: FnMut(&(usize, &'a str)) -> bool>(
        &mut self,
        mut predicate: F,
    ) -> &'a str {
        let s = self.as_str();
        if s.is_empty() {
            return s;
        }
        let start = self.position;
        let end =
            match itertools::Itertools::peeking_take_while(self, |(idx, s)| predicate(&(*idx, *s)))
                .last()
            {
                Some((end, _)) => match self.peek() {
                    Some((start, _)) => *start,
                    None => end + 1,
                },
                None => start,
            };
        self.position = end;
        &self.source[start..end]
    }

    pub fn as_str(&self) -> &'a str {
        let start = match self.peeked {
            Some(Some((i, _))) => i,
            _ => self.position,
        };
        &self.source[start..]
    }
}

impl<'a> Iterator for UnicodeString<'a> {
    type Item = (usize, &'a str);

    fn next(&mut self) -> Option<Self::Item> {
        let next = match self.peeked.take() {
            Some(p) => p,
            None => self.graphemes.next(),
        };

        if let Some((idx, _)) = next {
            self.position = idx;
        }
        next
    }
}

impl<'a> PeekingNext for UnicodeString<'a> {
    fn peeking_next<F>(&mut self, predicate: F) -> Option<Self::Item>
    where
        F: FnOnce(&Self::Item) -> bool,
    {
        match self.peek() {
            Some(v) => {
                if predicate(v) {
                    self.next()
                } else {
                    None
                }
            }
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_peek() {
        let mut ustr = UnicodeString::new("abc");
        assert_eq!(Some(&(0, "a")), ustr.peek());
        assert_eq!(0, ustr.tellp());

        assert_eq!(Some(&(0, "a")), ustr.peek());
        assert_eq!(0, ustr.tellp());
    }

    #[test]
    fn test_next() {
        let mut ustr = UnicodeString::new("abc");
        assert_eq!(Some((0, "a")), ustr.next());
        assert_eq!(0, ustr.tellp());

        assert_eq!(Some((1, "b")), ustr.next());
        assert_eq!(1, ustr.tellp());

        assert_eq!(Some((2, "c")), ustr.next());
        assert_eq!(2, ustr.tellp());

        assert_eq!(None, ustr.next());
        assert_eq!(2, ustr.tellp());
    }

    #[test]
    fn test_peek_and_next() {
        let mut ustr = UnicodeString::new("abc");
        assert_eq!(Some((0, "a")), ustr.next());
        assert_eq!(0, ustr.tellp());

        assert_eq!(Some(&(1, "b")), ustr.peek());
        assert_eq!(0, ustr.tellp());
        assert_eq!(Some(&(1, "b")), ustr.peek());
        assert_eq!(0, ustr.tellp());
        assert_eq!(Some((1, "b")), ustr.next());
        assert_eq!(1, ustr.tellp());

        assert_eq!(Some(&(2, "c")), ustr.peek());
        assert_eq!(1, ustr.tellp());
        assert_eq!(Some((2, "c")), ustr.next());
        assert_eq!(2, ustr.tellp());

        assert_eq!(None, ustr.next());
        assert_eq!(2, ustr.tellp());
    }

    #[test]
    pub fn test_peeking_take_while() {
        let mut ustr = UnicodeString::new("abc{def}");
        let s = ustr.peeking_take_while(|(_, c)| *c != "{");
        assert_eq!("abc", s);
        assert_eq!(Some(&(3, "{")), ustr.peek());
        assert_eq!(3, ustr.tellp());

        let mut ustr = UnicodeString::new(" ");
        let s = ustr.peeking_take_while(|(_, s)| !s.starts_with('\\'));
        assert_eq!(" ", s);
    }

    #[test]
    pub fn test_peeking_take_while_none() {
        let mut ustr = UnicodeString::new("abc{def}");
        let s = ustr.peeking_take_while(|(_, _)| false);
        assert_eq!("", s);
        assert_eq!(Some(&(0, "a")), ustr.peek());
        assert_eq!(0, ustr.tellp());
    }
}
