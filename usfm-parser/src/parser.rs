use crate::lexer::{Token, TokenIter, TokenKind};
use serde::Serialize;
use std::collections::HashMap;

#[derive(Debug, PartialEq, Serialize)]
#[serde(tag = "kind")]
pub enum Node<'a> {
    Tag {
        tag: &'a str,
    },
    Text {
        text: &'a str,
    },
    TextAndAttributes {
        text: &'a str,
        attributes: HashMap<&'a str, &'a str>,
    },
    TagAndText {
        tag: &'a str,
        text: &'a str,
    },
    RankedTag {
        tag: &'a str,
        rank: usize,
    },
    Group {
        parent: Box<Node<'a>>,
        children: Vec<Node<'a>>,
    },
    Pair {
        node1: Box<Node<'a>>,
        node2: Box<Node<'a>>,
    },
    List {
        children: Vec<Node<'a>>,
    },
}

fn split<'a>(s: &'a str, p: &str) -> (&'a str, &'a str) {
    match s.find(p) {
        Some(idx) => {
            let front = &s[0..idx];
            let back = &s[idx + p.len()..];
            (front, back)
        }
        None => (s, ""),
    }
}

fn text_with_attributes<'a>(lex: &mut TokenIter<'a>, opener: Token<'a>) -> Node<'a> {
    let t = lex.expect(TokenKind::Text);
    let (head, tail) = match t.text.find('|') {
        Some(idx) => (&t.text[..idx], &t.text[idx + 1..]),
        None => (t.text, ""),
    };

    let mut attributes = HashMap::new();
    for attr in tail.split_whitespace() {
        let (front, back) = split(attr, "=");
        attributes.insert(front, back);
    }
    let closing_w = lex.expect(TokenKind::Tag);
    assert_eq!(closing_w.text, format!("{}*", opener.text));
    Node::TextAndAttributes {
        text: head,
        attributes,
    }
}

fn text_pair<'a>(lex: &mut TokenIter<'a>, t: Token<'a>) -> Node<'a> {
    Node::Pair {
        node1: Box::new(Node::Tag { tag: t.text }),
        node2: Box::new(Node::Text {
            text: lex.expect(TokenKind::Text).text,
        }),
    }
}

fn tag_and_text<'a>(lex: &mut TokenIter<'a>, t: Token<'a>) -> Node<'a> {
    lex.ustr()
        .peeking_take_while(|(_, s)| s.chars().all(|c| c.is_whitespace()));
    let text = lex
        .ustr()
        .peeking_take_while(|(_, s)| s.chars().all(|c| c.is_digit(10)));
    Node::TagAndText { tag: t.text, text }
}

fn ranked_tag<'a>(_lex: &mut TokenIter, t: Token<'a>, rank: usize) -> Node<'a> {
    Node::RankedTag { tag: t.text, rank } // TODO
}

fn enclosed<'a>(lex: &mut TokenIter<'a>, t: Token<'a>) -> Node<'a> {
    let closer_text = format!("{}*", t.text);

    let enclosed_nodes = parse_list(lex, 0);

    lex.peek();
    assert_eq!(lex.expect(TokenKind::Tag).text, closer_text);
    Node::Group {
        parent: Box::new(Node::Tag { tag: t.text }),
        children: enclosed_nodes,
    }
}

fn prececence(t: &Token) -> usize {
    match t.kind {
        TokenKind::EOF => 0,
        TokenKind::Tag => match t.text {
            "\\bk*" => 0,
            "\\+bk*" => 0,
            "\\f*" => 0,
            "\\k*" => 0,
            "\\qs*" => 0,
            "\\w*" => 0,
            "\\+w*" => 0,
            "\\wj*" => 0,
            "\\x*" => 0,

            "\\c" => 1,
            "\\v" => 2,

            "\\p" => 3,
            "\\f" => 3,
            "\\w" => 3,

            _ => 1,
        },
        TokenKind::Text => 100,
    }
}

fn parse_single<'a>(lex: &mut TokenIter<'a>, t: Token<'a>) -> Option<Node<'a>> {
    let result: Node<'a> = match t.kind {
        TokenKind::EOF => return None,
        TokenKind::Text => Node::Text { text: t.text },
        TokenKind::Tag => match t.text {
            "\\c" => {
                let container = tag_and_text(lex, t.clone());
                Node::Group {
                    parent: Box::new(container),
                    children: parse_list(lex, prececence(&t)),
                }
            }
            "\\cp" => tag_and_text(lex, t),
            "\\v" => {
                let container = tag_and_text(lex, t.clone());
                Node::Group {
                    parent: Box::new(container),
                    children: parse_list(lex, prececence(&t)),
                }
            }

            "\\cl" => text_pair(lex, t),
            "\\d" => text_pair(lex, t),
            "\\id" => text_pair(lex, t),
            "\\ip" => text_pair(lex, t),
            "\\is1" => text_pair(lex, t),
            "\\ide" => text_pair(lex, t),
            "\\h" => text_pair(lex, t),
            "\\toc1" => text_pair(lex, t),
            "\\toc2" => text_pair(lex, t),
            "\\toc3" => text_pair(lex, t),
            "\\ms1" => text_pair(lex, t),
            "\\mt1" => text_pair(lex, t),
            "\\mt2" => text_pair(lex, t),
            "\\mt3" => text_pair(lex, t),
            "\\ili" => text_pair(lex, t),
            "\\ili2" => text_pair(lex, t),
            "\\pc" => text_pair(lex, t),
            "\\s1" => text_pair(lex, t),
            "\\sp" => text_pair(lex, t),

            "\\f" => enclosed(lex, t),
            "\\fl" => text_pair(lex, t),
            "\\fr" => text_pair(lex, t),
            "\\ft" => text_pair(lex, t),
            "\\fq" => text_pair(lex, t),
            "\\fqa" => text_pair(lex, t),

            "\\k" => enclosed(lex, t),

            "\\qs" => enclosed(lex, t),

            "\\w" => text_with_attributes(lex, t),
            "\\+w" => text_with_attributes(lex, t),

            "\\wj" => enclosed(lex, t),

            "\\x" => enclosed(lex, t),
            "\\xo" => text_pair(lex, t),
            "\\xt" => text_pair(lex, t),

            "\\bk" => enclosed(lex, t),
            "\\+bk" => enclosed(lex, t),

            "\\q1" => ranked_tag(lex, t, 1),
            "\\q2" => ranked_tag(lex, t, 2),
            "\\q3" => ranked_tag(lex, t, 2),
            "\\pi1" => ranked_tag(lex, t, 2),
            "\\li1" => ranked_tag(lex, t, 2),

            "\\b" => Node::Tag { tag: t.text },
            "\\m" => Node::Tag { tag: t.text },
            "\\mi" => Node::Tag { tag: t.text },
            "\\nb" => Node::Tag { tag: t.text },
            "\\p" => Node::Tag { tag: t.text },

            _ => todo!(
                "t={:#?} (next 50 chars: {})",
                t,
                lex.ustr().take(50).map(|(_, s)| s).collect::<String>()
            ),
        },
    };
    Some(result)
}

pub fn parse_list<'a>(lex: &mut TokenIter<'a>, rbp: usize) -> Vec<Node<'a>> {
    let mut nodes = vec![];

    loop {
        let t = lex.peek();
        if prececence(&t) <= rbp {
            break;
        }
        let t = lex.next(); // mark-read
        let node = match parse_single(lex, t) {
            Some(node) => node,
            None => break,
        };
        nodes.push(node);
    }

    nodes
}

pub fn parse<'a>(lex: &mut TokenIter<'a>) -> Node<'a> {
    Node::List {
        children: parse_list(lex, 0),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_precedence() {
        let mut lex = TokenIter::from_str("");
        let ast = parse_list(&mut lex, 0);
        assert_eq!(ast, vec![]);

        let mut lex = TokenIter::from_str(
            "\\c 1 \\p \\v 1 C1V1 \\f this is a footnote \\f* text \\v 2 C1V2 \\c 2 \\v 1 C2V1",
        );
        let ast = parse_list(&mut lex, 0);
        println!("ast={:#?}", ast);
        assert_eq!(
            ast,
            vec![
                Node::Group {
                    parent: Box::new(Node::TagAndText {
                        tag: "\\c",
                        text: "1",
                    }),
                    children: vec![
                        Node::Text { text: " " },
                        Node::Tag { tag: "\\p" },
                        Node::Text { text: " " },
                        Node::Group {
                            parent: Box::new(Node::TagAndText {
                                tag: "\\v",
                                text: "1"
                            }),
                            children: vec![
                                Node::Text { text: " C1V1 " },
                                Node::Group {
                                    parent: Box::new(Node::Tag { tag: "\\f" }),
                                    children: vec![Node::Text {
                                        text: " this is a footnote "
                                    }]
                                },
                                Node::Text { text: " text " },
                            ],
                        },
                        Node::Group {
                            parent: Box::new(Node::TagAndText {
                                tag: "\\v",
                                text: "2"
                            }),
                            children: vec![Node::Text { text: " C1V2 " }],
                        },
                    ],
                },
                Node::Group {
                    parent: Box::new(Node::TagAndText {
                        tag: "\\c",
                        text: "2"
                    }),
                    children: vec![
                        Node::Text { text: " " },
                        Node::Group {
                            parent: Box::new(Node::TagAndText {
                                tag: "\\v",
                                text: "1"
                            }),
                            children: vec![Node::Text { text: " C2V1" }],
                        },
                    ],
                },
            ]
        ); // TODO
    }
}
