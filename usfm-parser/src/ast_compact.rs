use serde::Serialize;
use std::{collections::HashMap, convert::TryFrom};

#[derive(Debug, PartialEq, Serialize)]
pub enum Tag {
    PlusBK,

    B,
    BK,
    C,
    CL,
    CP,
    D,
    F,
    FL,
    FR,
    FT,
    FQ,
    FQA,
    H,
    ID,
    IDE,
    ILI,
    ILI2,
    IP,
    IS1,
    K,
    LI1,
    M,
    MI,
    MS1,
    MT1,
    MT2,
    MT3,
    NB,
    P,
    PC,
    PI1,
    Q1,
    Q2,
    Q3,
    QS,
    S1,
    SP,
    TOC1,
    TOC2,
    TOC3,
    V,
    WJ,
    X,
    XO,
    XT,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize)]
pub enum AttributeKey {
    Strong,
}

#[derive(Debug, PartialEq, Serialize)]
pub enum AttributeValue {
    H(u16),
    G(u16),
}

#[derive(Debug, PartialEq, Serialize)]
pub enum Node<'a> {
    Tag(Tag),
    Text(&'a str),
    TextAndAttributes(&'a str, HashMap<AttributeKey, AttributeValue>),
    TagAndText(Tag, &'a str),
    RankedTag(Tag, usize),
    Group(Box<Node<'a>>, Vec<Node<'a>>),
    Pair(Box<Node<'a>>, Box<Node<'a>>),
    List(Vec<Node<'a>>),
}

impl<'a> TryFrom<&crate::parser::Node<'a>> for Node<'a> {
    type Error = String;

    fn try_from(node: &crate::parser::Node<'a>) -> Result<Self, Self::Error> {
        fn str_to_tag(tag: &str) -> Result<Tag, String> {
            let result = match tag {
                "\\+bk" => Tag::PlusBK,

                "\\b" => Tag::B,
                "\\bk" => Tag::BK,
                "\\c" => Tag::C,
                "\\cl" => Tag::CL,
                "\\cp" => Tag::CP,
                "\\d" => Tag::D,
                "\\f" => Tag::F,
                "\\fl" => Tag::FL,
                "\\fr" => Tag::FR,
                "\\ft" => Tag::FT,
                "\\fq" => Tag::FQ,
                "\\fqa" => Tag::FQA,
                "\\h" => Tag::H,
                "\\id" => Tag::ID,
                "\\ide" => Tag::IDE,
                "\\ili" => Tag::ILI,
                "\\ili2" => Tag::ILI2,
                "\\ip" => Tag::IP,
                "\\is1" => Tag::IS1,
                "\\k" => Tag::K,
                "\\li1" => Tag::LI1,
                "\\m" => Tag::M,
                "\\mi" => Tag::MI,
                "\\ms1" => Tag::MS1,
                "\\mt1" => Tag::MT1,
                "\\mt2" => Tag::MT2,
                "\\mt3" => Tag::MT3,
                "\\nb" => Tag::NB,
                "\\p" => Tag::P,
                "\\pc" => Tag::PC,
                "\\pi1" => Tag::PI1,
                "\\q1" => Tag::Q1,
                "\\q2" => Tag::Q2,
                "\\q3" => Tag::Q3,
                "\\qs" => Tag::QS,
                "\\s1" => Tag::S1,
                "\\sp" => Tag::SP,
                "\\toc1" => Tag::TOC1,
                "\\toc2" => Tag::TOC2,
                "\\toc3" => Tag::TOC3,
                "\\v" => Tag::V,
                "\\wj" => Tag::WJ,
                "\\x" => Tag::X,
                "\\xo" => Tag::XO,
                "\\xt" => Tag::XT,
                t => return Err(format!("unknown tag: {}", t)),
            };
            Ok(result)
        }

        let node = match node {
            crate::parser::Node::Tag { tag } => Node::Tag(str_to_tag(*tag)?),

            crate::parser::Node::Text { text } => Node::Text(text),

            crate::parser::Node::Pair { node1, node2 } => Node::Pair(
                Box::new(Node::try_from(node1.as_ref())?),
                Box::new(Node::try_from(node2.as_ref())?),
            ),

            crate::parser::Node::List { children } => Node::List(
                children
                    .iter()
                    .map(Node::try_from)
                    .collect::<Result<Vec<_>, String>>()?,
            ),

            crate::parser::Node::Group { parent, children } => Node::Group(
                Box::new(Node::try_from(parent.as_ref())?),
                children
                    .iter()
                    .map(Node::try_from)
                    .collect::<Result<Vec<_>, String>>()?,
            ),

            crate::parser::Node::RankedTag { tag, rank } => {
                Node::RankedTag(str_to_tag(tag)?, *rank)
            }

            crate::parser::Node::TagAndText { tag, text } => {
                Node::TagAndText(str_to_tag(*tag)?, text)
            }

            crate::parser::Node::TextAndAttributes { text, attributes } => {
                let mut attrs = HashMap::new();
                for (k, v) in attributes {
                    let k = match *k {
                        "strong" => AttributeKey::Strong,
                        k => return Err(format!("unknown attribute key: {:?}", k)),
                    };

                    let v = v.trim_start_matches('"').trim_end_matches('"');
                    if !v.starts_with('G') && !v.starts_with('H') {
                        return Err(format!("unknown attribute value: {:?}", v));
                    }

                    let g_or_h = &v[0..1];
                    let v = &v[1..];
                    let rank = v
                        .parse()
                        .map_err(|e| format!("could not convert to number: {:?}: {}", v, e))?;

                    attrs.insert(
                        k,
                        match g_or_h {
                            "G" => AttributeValue::G(rank),
                            "H" => AttributeValue::H(rank),
                            _ => unreachable!(),
                        },
                    );
                }
                Node::TextAndAttributes(text, attrs)
            }
        };
        Ok(node)
    }
}

impl<'a> Node<'a> {
    fn to_capnp() {
        use usfm_serializer;

    }
}
